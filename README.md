## Multi-threaded file reader & occurrence diagrams finder
multi-threaded test case for reading a file and creating the
histogram of diagrams (sequence of 2 letters) in a large file.

## Description
Given a large file and the number of available threads,
divide into chunks of lines each thread processes,
and find the total number each sequence of 2 letters appears. 

## RUN
for running main:
g++ findDiagrams.cpp -o findDiagrams
./findDiagrams
