/*
This file is part of multi-threaded file analyzer.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Copyright
    N.K. 2023
*/

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <thread>
#include <mutex>

using namespace std;

class diagram
{
    //class members
    unordered_map<string, int>& histogram;
    mutex& mutexd;
    int startLine;
    int endLine;
    
public:
    //class members
    static string filename;
    
    //class member functions
        
    //constructor
    diagram
    (
        unordered_map<string, int>& histogram, 
        mutex& mutex,
        int startLine, 
        int endLine
    )
    : 
    histogram(histogram), 
    mutexd(mutex), 
    startLine(startLine), 
    endLine(endLine)
    {}

    static int checkFile(const ifstream& file)
    {
        if(!file.is_open()) 
        {
            cerr << "Error opening file: " << filename << endl;
            return 1;
        }
        else
        {
            return 0;
        }
    }

    void processFile() 
    {
        ifstream file(filename);
        checkFile(file);
        string line;
        int currentLine = 0;
        while (getline(file, line) && currentLine<endLine) 
        {
            if (currentLine>=startLine) 
            {
                processLine(line);
            }
            ++currentLine;
        }
        file.close();
    }

    static int getNumLines()
    {
        ifstream file(filename);
        checkFile(file);
        int totalLines = 0;
        string line;
        while (getline(file, line)) 
        {
            ++totalLines;
        }
        return totalLines;
    }
    
    static void closeFile()
    {
        ifstream file(filename);
        file.close();
    }
    
private:

    void processLine(const string& line) 
    {
        for (size_t i=0; i<line.length()-1; ++i) 
        {
            if (isalpha(line[i]) && isalpha(line[i+1])) 
            {
                string diagram = line.substr(i, 2);
                mutexd.lock();
                histogram[diagram]++;
                mutexd.unlock();
            }
        }
    }

};

// initialise static member
string diagram::filename="myFile.dat"; 

int main()
{
    const int numThreads = 16;
    unordered_map<string, int> histogram;
    mutex mutexm;
    vector<thread> ths; 
    vector<diagram> histogramThreads;

    // Get the total number of lines in the file
    const int totalLines(diagram::getNumLines()); 
    cout << "Found " << totalLines << " lines in file " << diagram::filename << endl;

    // Divide lines evenly among threads
    int linesPerThread = totalLines / numThreads;
    int remainingLines = totalLines % numThreads;
    int startLine = 0;
    int endLine = 0;

    for (int i=0; i<numThreads; ++i) 
    {
        endLine = startLine + linesPerThread +(i < remainingLines ? 1 : 0);
        // histogramThreads.emplace_back(filename, histogram, mutexm, startLine, endLine);
        diagram diagr(histogram, mutexm, startLine, endLine);
        histogramThreads.push_back(diagr);
        startLine = endLine;
    }

    diagram::closeFile();

    // create threads
    for (int i = 0; i < numThreads; ++i) 
    {
        ths.push_back
        (
            thread
            (
                &diagram::processFile, 
                histogramThreads[i]
            )
        );
    }
    
    for(auto& th: ths)
    {
        th.join();
    }

    // Print diagram occurrences
    for (const auto& entry:histogram) 
    {
        cout << entry.first << ": " << entry.second << endl;
    }

    return 0;
}
